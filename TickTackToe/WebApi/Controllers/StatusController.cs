﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    public class StatusController : Controller
    {
        [HttpPost(nameof(Online))]
        public IActionResult Online()
        {
            return View();
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Content("value1 value2 status");
        }
    }
}