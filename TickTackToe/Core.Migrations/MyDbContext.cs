﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Core.Migrations
{
    public class MyDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string cn = @"Server=DESKTOP-NVOHN59\\SQLEXPRESS;Database=SomeTest;Trusted_Connection=True;MultipleActiveResultSets=true";
            optionsBuilder.UseSqlServer(cn);

            base.OnConfiguring(optionsBuilder);
        }
    }
}
