"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pathJoin = function (pathArr) { return pathArr
    .map(function (path) {
    var result = path;
    if (result === '/') {
        result = result.slice(1);
    }
    if (result[result.length - 1] === '/') {
        result = result.slice(0, result.length - 1);
    }
    return result;
})
    .join('/'); };
//# sourceMappingURL=pathJoin.js.map