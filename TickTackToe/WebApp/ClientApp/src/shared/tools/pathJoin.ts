﻿export const pathJoin = (pathArr: string[]): string => pathArr
    .map((path) => {
        let result: string = path;
        if (result === '/') {
            result = result.slice(1);
        }
        if (result[result.length - 1] === '/') {
            result = result.slice(0, result.length - 1);
        }
        return result;
    })
    .join('/');