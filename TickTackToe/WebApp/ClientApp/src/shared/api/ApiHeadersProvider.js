"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.JSON_HEADERS = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
};
var HeadersProvider = /** @class */ (function () {
    function HeadersProvider(authTokenProvider) {
        this.authTokenProvider = authTokenProvider;
    }
    Object.defineProperty(HeadersProvider.prototype, "authHeaders", {
        get: function () {
            if (this.authTokenProvider.token) {
                return {
                    Authorization: "Bearer " + this.authTokenProvider.token
                };
            }
            return {};
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HeadersProvider.prototype, "headers", {
        get: function () {
            return __assign({}, exports.JSON_HEADERS, this.authHeaders);
        },
        enumerable: true,
        configurable: true
    });
    return HeadersProvider;
}());
exports.HeadersProvider = HeadersProvider;
//# sourceMappingURL=ApiHeadersProvider.js.map