﻿import { AuthTokenProvider } from './models/AuthTokenProvider';

export interface ApiHeadersProvider {
    readonly headers: Record<string, string>;
}

export const JSON_HEADERS: Record<string, string> = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
};

export class HeadersProvider implements ApiHeadersProvider {
    private readonly authTokenProvider: AuthTokenProvider;

    constructor(authTokenProvider: AuthTokenProvider) {
        this.authTokenProvider = authTokenProvider;
    }

    private get authHeaders(): Record<string, string> {
        if (this.authTokenProvider.token) {
            return {
                Authorization: `Bearer ${this.authTokenProvider.token}`
            };
        }
        return {};
    }

    get headers(): Record<string, string> {
        return { ...JSON_HEADERS, ...this.authHeaders };
    }
}