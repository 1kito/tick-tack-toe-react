﻿import { ApiHeadersProvider } from './ApiHeadersProvider';
import { HttpClient } from './httpClient/HttpClient';
import { httpResponseHandler } from '../models/httpResponseHandler';

export type ApiResponseHandler = (response: Response) => unknown;

export class ApiClient {
    private readonly client: HttpClient;

    private readonly responseHandler: ApiResponseHandler;

    private readonly headersProvider: ApiHeadersProvider;

    constructor(
        client: HttpClient,
        headersProvider: ApiHeadersProvider,
        responseHandler?: ApiResponseHandler,
    ) {
        this.client = client;
        this.headersProvider = headersProvider;
        this.responseHandler = responseHandler || httpResponseHandler;
    }

    get<TResult = unknown>(
        path: string,
        params?: Record<string, string>,
    ): Promise<TResult> {
        return this.client
            .get(path, params, this.headersProvider.headers)
            .then(this.responseHandler) as Promise<TResult>;
    }

    post<TResult = unknown>(
        path: string,
        body?: object,
        params?: Record<string, string>,
    ): Promise<TResult> {
        return this.client
            .post(path, body, params, this.headersProvider.headers)
            .then(this.responseHandler) as Promise<TResult>;
    }

    put<TResult = unknown>(
        path: string,
        body?: object,
        params?: Record<string, string>,
    ): Promise<TResult> {
        return this.client
            .put(path, body, params, this.headersProvider.headers)
            .then(this.responseHandler) as Promise<TResult>;
    }

    delete<TResult = unknown>(
        path: string,
        params?: Record<string, string>,
    ): Promise<TResult> {
        return this.client
            .delete(path, params, this.headersProvider.headers)
            .then(this.responseHandler) as Promise<TResult>;
    }
}

