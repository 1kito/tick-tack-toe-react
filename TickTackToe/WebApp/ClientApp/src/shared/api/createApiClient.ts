﻿import { ApiHeadersProvider } from './ApiHeadersProvider';
import { ApiResponseHandler, ApiClient } from './ApiClient';
import { HttpClient } from './httpClient';

export function createApiClient(
    headersProvider: ApiHeadersProvider,
    baseUrl?: string,
    responseHandler?: ApiResponseHandler,
): ApiClient {
    const httpClient = new HttpClient(baseUrl);
    const apiClient = new ApiClient(httpClient, headersProvider, responseHandler);
    return apiClient;
}