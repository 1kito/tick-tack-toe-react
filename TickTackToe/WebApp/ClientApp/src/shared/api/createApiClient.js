"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiClient_1 = require("./ApiClient");
var httpClient_1 = require("./httpClient");
function createApiClient(headersProvider, baseUrl, responseHandler) {
    var httpClient = new httpClient_1.HttpClient(baseUrl);
    var apiClient = new ApiClient_1.ApiClient(httpClient, headersProvider, responseHandler);
    return apiClient;
}
exports.createApiClient = createApiClient;
//# sourceMappingURL=createApiClient.js.map