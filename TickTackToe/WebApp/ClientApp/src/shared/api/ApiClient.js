"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var httpResponseHandler_1 = require("../models/httpResponseHandler");
var ApiClient = /** @class */ (function () {
    function ApiClient(client, headersProvider, responseHandler) {
        this.client = client;
        this.headersProvider = headersProvider;
        this.responseHandler = responseHandler || httpResponseHandler_1.httpResponseHandler;
    }
    ApiClient.prototype.get = function (path, params) {
        return this.client
            .get(path, params, this.headersProvider.headers)
            .then(this.responseHandler);
    };
    ApiClient.prototype.post = function (path, body, params) {
        return this.client
            .post(path, body, params, this.headersProvider.headers)
            .then(this.responseHandler);
    };
    ApiClient.prototype.put = function (path, body, params) {
        return this.client
            .put(path, body, params, this.headersProvider.headers)
            .then(this.responseHandler);
    };
    ApiClient.prototype.delete = function (path, params) {
        return this.client
            .delete(path, params, this.headersProvider.headers)
            .then(this.responseHandler);
    };
    return ApiClient;
}());
exports.ApiClient = ApiClient;
//# sourceMappingURL=ApiClient.js.map