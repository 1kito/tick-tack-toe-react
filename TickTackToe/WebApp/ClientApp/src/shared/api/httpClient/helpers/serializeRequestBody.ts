﻿export function serializeRequestBody(
    body?: string | object | Blob | FormData,
): string | Blob | FormData | null {
    if (body == null) {
        return null;        
    }

    if (body instanceof Blob || body instanceof FormData || typeof body === 'string') {
        return body;
    }

    return JSON.stringify(body);
}