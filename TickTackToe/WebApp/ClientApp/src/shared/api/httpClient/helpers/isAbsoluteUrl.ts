﻿const urlPattern = /^https?:\/\//i;

export function isAbsoluteUrl(urlOrPath: string): boolean {
    return urlPattern.test(urlOrPath);
}