﻿export function buildQueryParams(params?: Record<string, string>): string {
    if (!params) {
        return '';
    }
    const paramsKeys = Object.keys(params);
    if (paramsKeys.length === 0) {
        return '';
    }

    return paramsKeys
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
        .join('&');
}