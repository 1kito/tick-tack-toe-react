"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var urlPattern = /^https?:\/\//i;
function isAbsoluteUrl(urlOrPath) {
    return urlPattern.test(urlOrPath);
}
exports.isAbsoluteUrl = isAbsoluteUrl;
//# sourceMappingURL=isAbsoluteUrl.js.map