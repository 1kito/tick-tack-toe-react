"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function serializeRequestBody(body) {
    if (body == null) {
        return null;
    }
    if (body instanceof Blob || body instanceof FormData || typeof body === 'string') {
        return body;
    }
    return JSON.stringify(body);
}
exports.serializeRequestBody = serializeRequestBody;
//# sourceMappingURL=serializeRequestBody.js.map