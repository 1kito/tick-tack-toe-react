"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function buildQueryParams(params) {
    if (!params) {
        return '';
    }
    var paramsKeys = Object.keys(params);
    if (paramsKeys.length === 0) {
        return '';
    }
    return paramsKeys
        .map(function (key) { return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]); })
        .join('&');
}
exports.buildQueryParams = buildQueryParams;
//# sourceMappingURL=buildQueryParams.js.map