﻿import {
    isAbsoluteUrl,
    serializeRequestBody,
    buildQueryParams,
} from './helpers';
import { pathJoin } from 'tools';

function buildRequestInit(
    method: 'GET' | 'POST' | 'PUT' | 'DELETE',
    headers?: Record<string, string>,
    body?: object | string,
): RequestInit {
    return {
        method,
        headers,
        body: serializeRequestBody(body),
    };
}

export class HttpClient {
    private baseURL: string;

    constructor(baseUrl?: string) {
        this.baseURL = baseUrl || '';
    }

    private buildUrl(path: string, params?: Record<string, string>): string {
        const queryParams = params ? `?${buildQueryParams(params)}` : '';
        if (isAbsoluteUrl(path) || !this.baseURL) {
            return path + queryParams;
        }
        const url = pathJoin([this.baseURL, path]);
        return url + queryParams;
    }

    get(
        path: string,
        params?: Record<string, string>,
        headers?: Record<string, string>,
    ): Promise<Response> {
        return fetch(this.buildUrl(path, params), buildRequestInit('GET', headers));
    }

    post(
        path: string,
        body?: object,
        params?: Record<string, string>,
        headers?: Record<string, string>,
    ): Promise<Response> {
        return fetch(
            this.buildUrl(path, params),
            buildRequestInit('POST', headers, body),
        );
    }

    put(
        path: string,
        body?: string | object,
        params?: Record<string, string>,
        headers?: Record<string, string>,
    ): Promise<Response> {
        return fetch(
            this.buildUrl(path, params),
            buildRequestInit('PUT', headers, body),
        );
    }

    delete(
        path: string,
        params?: Record<string, string>,
        headers?: Record<string, string>,
    ): Promise<Response> {
        return fetch(
            this.buildUrl(path, params),
            buildRequestInit('DELETE', headers),
        );
    }
}


