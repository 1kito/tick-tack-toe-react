﻿export class ApiError extends Error {
    readonly httpStatus: number;

    constructor(httpStatus: number, message?: string) {
        super(message);
        this.httpStatus = httpStatus;
    }
}