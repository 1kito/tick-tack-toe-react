﻿export interface AuthTokenProvider {
    readonly token: string | null;
}