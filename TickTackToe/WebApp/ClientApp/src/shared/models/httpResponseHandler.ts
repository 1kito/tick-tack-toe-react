﻿import { ApiError } from './ApiError';

export function httpResponseHandler(response: Response): Promise<unknown> {
    if (!response.ok) {
        throw new ApiError(response.status, response.statusText);
    }
    // no content 
    if (response.status === 204) {
        return Promise.resolve(null);
    }

    return response.json();
}