﻿import { ApiClient } from '../../shared/api/ApiClient';

const CONTROLLER_NAME = 'Status';

export interface StatusController {
    online: () => Promise<void>;
    oncall: () => Promise<void>;
    telephone: () => Promise<void>;
    away: () => Promise<void>;
}

export function statusController(api: ApiClient): StatusController {
    return {
        online: () => api.post<void>(`${CONTROLLER_NAME}/online`),
        oncall: () => api.post<void>(`${CONTROLLER_NAME}/oncall`),
        telephone: () => api.post<void>(`${CONTROLLER_NAME}/telephone`),
        away: () => api.post<void>(`${CONTROLLER_NAME}/away`),
    }
}