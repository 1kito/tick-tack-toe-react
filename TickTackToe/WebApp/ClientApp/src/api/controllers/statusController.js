"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CONTROLLER_NAME = 'Status';
function statusController(api) {
    return {
        online: function () { return api.post(CONTROLLER_NAME + "/online"); },
        oncall: function () { return api.post(CONTROLLER_NAME + "/oncall"); },
        telephone: function () { return api.post(CONTROLLER_NAME + "/telephone"); },
        away: function () { return api.post(CONTROLLER_NAME + "/away"); },
    };
}
exports.statusController = statusController;
//# sourceMappingURL=statusController.js.map