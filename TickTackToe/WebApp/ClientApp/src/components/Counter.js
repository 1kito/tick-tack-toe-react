import React, { Component } from 'react';

export class Counter extends Component {
    static displayName = Counter.name;

    constructor(props) {
        super(props);
        this.incrementCounter = this.incrementCounter.bind(this);
        this.decrementCounter = this.decrementCounter.bind(this);
        
        let dt = this.getCurrentDateTime();
        this.state = { currentCount: 1000, updated: dt, time: '' };
    }

    incrementCounter() {
        this.updateDate();
        this.setState({
            currentCount: this.state.currentCount + 1,
        });
    }

    decrementCounter() {
        this.updateDate();
        this.setState({
            currentCount: this.state.currentCount - 1
        });
    }

    getCurrentTime() {
        let d = new Date();
        return d.toTimeString();
    }

    getCurrentDateTime() {
        let d = new Date();
        return d.toDateString() + ' ' + d.toTimeString();
    }

    updateDate() {
        this.setState({
            updated: this.getCurrentDateTime()
        });
    }

    componentDidMount() {
        this.interval = setInterval(() => this.setState({ time: this.getCurrentTime() }), 1000);
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div>
                <h1>Counter</h1>

                <p>This is a simple example of a React component.</p>

                <p>Current count: <strong>{this.state.currentCount}</strong>
                    Updated: <b>{this.state.updated}</b>                    
                </p>
                <h2>{this.state.time}</h2>
                <button className="btn btn-primary" onClick={this.incrementCounter}>Increment</button>
                <button className="btn btn-primary" onClick={this.decrementCounter}>Decrement</button>
            </div>
        );
    }
}
